import csv

def read_data():
    f = open("rainfall.csv" ,"r") 
    listOfDictionaries = [ {k: float(v) for k,v in row.items()} for row in csv.DictReader(f)]
    f.close()
    for item in listOfDictionaries:
        item['year'] = int(item['year'])
        item['id']   = int(item['id'])
    return listOfDictionaries


def dates(data, start=None, end=None):
    
    sIndex = 0
    eIndex = 4
 
    sIndex = next((index for (index,dictionary) in enumerate(data) if dictionary["year"]==start), None)
    eIndex = next((index for (index,dictionary) in enumerate(data) if dictionary["year"]-1==end), None)
    
    return data[sIndex:eIndex]


def paginate(data, offset=None, limit=None):
    
    sIndex = 0
    eIndex = 4

    if offset!=None:
        sIndex = offset

    if limit!=None:
        eIndex = sIndex+limit
    
    return data[sIndex:eIndex]




#data = read_data()
#print(paginate(data,offset=1,limit=2))
